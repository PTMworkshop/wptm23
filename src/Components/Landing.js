import React from 'react';

export default function Landing(props) {

    let intro1 = 'The Workshop on Privacy Threat Modeling brings together researchers, practitioners, government representatives, and industry specialists to collaborate on the topic of privacy threats. While aspects of privacy risk modeling are relatively well-developed, such as constructions of privacy harms (Solove’s Taxonomy of Privacy and Calo’s subjective/objective privacy harms for instance), there has been insufficient discussion around approaches to modeling privacy threats, broadly construed.';
    let intro2 = 'The workshop will include an informative session and a collaborative session. The informative session will consist of a keynote, brief updates on two privacy threat models (MITRE PANOPTIC™ and LINDDUN), and presentations selected from participant submissions. In the collaborative session we will discuss threat model generalization and operationalization in break-out groups, then come together to share discussion outcomes.'
    let intro3 = 'We will address how the community assesses the relative appropriateness of different degrees of privacy threat model generalization (from specific to global) and how to operationalize privacy threat models for threat assessment, risk modeling, and red teaming.'
    let intro4 = 'Broadly, we hope this workshop will forge new relationships across the privacy community around the topic of privacy threats, shape and guide the development of new and existing privacy threat models, and stimulate further research into privacy threats by providing an informative baseline on what privacy threat modeling is, as well as knowledge about the current state of the field, and ideas about operationalizing privacy threat models.'
    let topics = ['Definitions of a privacy incident, attack, threat, and breach', 
                'Distinctions between privacy threats, privacy harms, and privacy vulnerabilities', 
                'Describing or categorizing privacy threats, including taxonomies or ontologies for privacy incidents, attacks, threats, and breaches',
                'Applicability and limitations of security threat modeling techniques for privacy',
                'Integration of threat models in privacy risk models and risk management',
                'Privacy threat-informed defense',
                'Qualitative versus quantitative threat modeling',
                'Trade-offs between specific and general models',
                'Operationalizing privacy threat models',
                'Privacy attack case studies'];
    let contact1 = 'Please email  '
    let contact2 = ' to be added to our WPTM listserv.';
    

    return (
        <div className='base-page'>
            <h1 className='workshop-title'>The 2<sup>nd</sup> Workshop on Privacy Threat Modeling (WPTM)</h1>
            <h3 className='date-time-location'>Sunday, August 6th, 2023 | 2:30-6PM | In-person attendance</h3>
            <div className='intro-pars'>
                <p>{intro1}</p>
                <p>{intro2}</p>
                <p>{intro3}</p>
                <p>{intro4}</p>
                <p>Topics of interest include:</p>
                <ul className='bulleted-list'>
                {topics.map((topic, index) => (
                    <li>{topic}</li>
                ))}
                </ul>
            </div>
            <div className='new-section'>
                <h4 className='heading'>Important Dates</h4>
                <p>May 1, 2023 – Submissions open for presentation proposals</p>
                <p>June 1, 2023 23:59 AOE – Submission deadline</p>
                <p>June 8, 2023 – Decisions announced </p>
                <p>August 6, 2023 – The 2<sup>nd</sup> Workshop on Privacy Threat Modeling</p>
            </div>
            <div className='new-section'>
                <p><b>{contact1}
                <a className='link' href='mailto:PTMworkshop@mitre.org'>PTMworkshop@mitre.org</a>
                {contact2}</b></p>
            </div>
        </div>
    )
}