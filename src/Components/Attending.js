import React from 'react';

export default function Attending(props) {
    let attend1 = 'The Workshop on Privacy Threat Modeling will be held in conjunction with the ';
    let attend2 = ', August 6-8 in Anaheim, CA. ';
    let attend2n = 'You must register for both SOUPS and the workshop to attend. The workshop registration fee is US $75.'
    let attend3 = 'The workshop will take place ';
    let attend4 = 'in person on Sunday, August 6th from 2:30-6PM';
    let attend5 = '. Please check back for specific location information – it will be updated as soon available.';
    let attend6 = 'Attendees may submit a 1-page presentation proposal to be considered for a formal speaking role. Please see ';
    let attend7 = ' for details.';
    // let attend8 = 'For more information on SOUPS and to register:';
    let hybrid = 'The workshop will be held in person without remote attendees unless there is a change in public health recommendations for group settings. In this case, the workshop will be shortened and transitioned to a fully online format.'
    let reg_close = 'Registration has closed.'

    return (
        <div className='base-page'>
            <div className='attending-details'>
                <h4 className='heading'>Attendance</h4>
                <p><strong>{reg_close}</strong></p>
                <hr style={{marginTop:'25px', marginBottom:'25px'}}></hr>

                <p>{attend1} 
                <a className='link' href='https://www.usenix.org/conference/soups2023' target='_blank' rel="noopener noreferrer">Symposium on Usable Privacy and Security (SOUPS) 2023</a>
                {attend2}
                <strong>{attend2n}</strong>
                </p>

                <p>{attend3}
                <strong>{attend4}</strong>
                {attend5}
                </p>
                <p>{attend6}
                <a className='link' href='/wptm23/#/submissions'>Submissions</a>
                {attend7}
                </p>
            </div>
            <div className='new-section'>
                <h4 className='heading'>Hybrid Status</h4>
                <p>{hybrid}</p>
            </div>
            <div className='new-section'>
                <h4 className='heading'>Important Dates</h4>
                <p>May 1, 2023 – Submissions open for presentation proposals</p>
                <p>June 1, 2023 23:59 AOE – Submission deadline</p>
                <p>June 8, 2023 – Decisions announced </p>
                <p>August 6, 2023 – The 2<sup>nd</sup> Workshop on Privacy Threat Modeling</p>
            </div>
        </div>
    )
}