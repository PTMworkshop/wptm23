import React from 'react';

export default function Submissions(props) {
    // let sub_open = 'The submission portal is now open:';
    let sub1 = 'We are soliciting short presentation proposals (1 page using the SOUPS formatting template, found '
    let sub2 = ') related to privacy attacks, threats, and threat modeling. These may reflect work at any stage of development, including previously published results, and address:'
    let subList = ['Positions, arguments, or perspectives',
                    'Processes, methodologies, or solutions',
                    'Novel concepts',
                    'Case studies']
    let sub3 = 'Submissions should include author names and affiliations. Presentation abstracts will be made available to attendees prior to the workshop, and slides will be posted to ';
    let sub4 = ' following the workshop. These submissions will not be considered “published” work and should not preclude publication elsewhere.'
    let sub5 = 'Please email ';
    let sub6 = ' with questions.';
    // let reg1 ='Registration is now open and required for workshop attendance! Please see ';
    // let reg2 = ' for more information and to register.';
    let sub_close = 'The submission portal has closed.'
    
    return (
        <div className='base-page'>
            <div className='submission-details'>
                <h4 className='heading'>Submissions</h4>
                <p><strong>{sub_close}</strong></p>
                <hr style={{marginTop:'25px', marginBottom:'25px'}}></hr>

                <p> {sub1}
                <a className='link' href='https://www.usenix.org/conference/soups2023/call-for-papers' target='_blank' rel="noopener noreferrer">here</a>
                {sub2}
                </p>
                <ul className='bulleted-list'>
                {subList.map((subType, index) => (
                    <li>{subType}</li>
                ))}
                </ul>
                <p>{sub3}
                <a className='link' href='/wptm23/#/proceedings'>Proceedings</a>
                {sub4}
                </p>
                <p>{sub5}
                <a className='link' href='mailto:PTMworkshop@mitre.org'>PTMworkshop@mitre.org</a>
                {sub6}
                </p>
            </div>
            <div className='new-section'>
                <h4 className='heading'>Program Committee</h4>
                <p>Cara Bloom, Netflix</p>
                <p>Jessica Colnago, Google</p>
                <p>Jason Cronk, Enterprivacy Consulting Group</p>
                <p>Stuart Shapiro, MITRE</p>
                <p>Laurens Sion, KU Leuven</p>
            </div>
            <div className='new-section'>
                <h4 className='heading'>Important Dates</h4>
                <p>May 1, 2023 – Submissions open for presentation proposals</p>
                <p>June 1, 2023 23:59 AOE – Submission deadline</p>
                <p>June 8, 2023 – Decisions announced </p>
                <p>August 6, 2023 – The 2<sup>nd</sup> Workshop on Privacy Threat Modeling</p>
            </div>
        </div>
    )
}