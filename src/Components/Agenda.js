import React from "react";
import { useTable } from "react-table";

export default function Agenda(props) {

    const data = React.useMemo(
        () => [
          {
            time: '2:30-2:35',
            activity: 'Introduction and welcome',
          },
          {
            time: '2:35-3:15',
            activity: 'Keynote: Zach Miller, Meta',
          },
          {
            time: '3:15-3:25',
            activity: 'LINDDUN update',
          },
          {
            time: '3:25-3:35',
            activity: 'MITRE PANOPTIC™ update',
          },
          {
            time: '3:35-3:45',
            activity: 'Break',
          },
          {
            time: '3:45-4:05',
            activity: 'Presentation 1: Samantha Katcher, "Privacy Threat Modeling in the Context of Medical Devices: A Case Study"',
          },
          {
            time: '4:05-4:25',
            activity: 'Presentation 2: Jayati Dev, Bahman Rashidi, and Vaibhav Garg, "Models of Applied Privacy (MAP): A Persona-based Approach to Privacy Threat Modeling"',
          },
          {
            time: '4:25-4:45',
            activity: 'Presentation 3: Tianyi Shan, Mary Anne Smart, Jay Jhaveri, Jessalyn Wang, and Kristen Vaccaro, "Misalignments Between User and Developer Privacy Threat Models"',
          },
          {
            time: '4:45-4:55',
            activity: 'Break',
          },
          {
            time: '4:55-5:30',
            activity: 'Break-out sessions',
          },
          {
            time: '5:30-6:00',
            activity: 'Full group discussion of break-out sessions',
          },
          {
            time: '6:00',
            activity: 'Closing',
          },
        ],
        []
      )
    
      const columns = React.useMemo(
        () => [
          {
            Header: 'Time (PDT)',
            accessor: 'time',
          },
          {
            Header: 'Activity',
            accessor: 'activity',
          },
        ],
        []
      )
    
      const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
      } = useTable({ columns, data })

    return (
        <div>
            <table {...getTableProps()} style={{border: 'none', borderBottm: 'solid 1px #DFDFDF', width: '100%' }}>
            <thead>
                {headerGroups.map(headerGroup => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map(column => (
                    <th
                        {...column.getHeaderProps()}
                        style={{
                        borderBottom: 'solid 2px #4169E1',
                        background: 'white',
                        color: 'black',
                        fontWeight: 'bold',
                        }}
                        className='columns'
                    >
                        {column.render('Header')}
                    </th>
                    ))}
                </tr>
                ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                {rows.map(row => {
                prepareRow(row)
                return (
                    <tr {...row.getRowProps()}>
                    {row.cells.map(cell => {
                        if (cell.row.id % 2 !== 0) {
                            return (
                                <td
                                    {...cell.getCellProps()}
                                    style={{
                                    padding: '5px 70px 5px 5px',
                                    border: 'solid 1px #4169E1',
                                    background: 'white',
                                    }}
                                >
                                    {cell.render('Cell')}
                                </td>
                            )
                        } else {
                            return (
                                <td
                                    {...cell.getCellProps()}
                                    style={{
                                    padding: '5px 70px 5px 5px',
                                    border: 'solid 1px #4169E1',
                                    }}
                                >
                                    {cell.render('Cell')}
                                </td>
                            )
                        }
                        
                    })}
                    </tr>
                )
                })}
            </tbody>
            </table>
        </div>
    )
}