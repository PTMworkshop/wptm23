import React from 'react';
import panoptic_v1 from "../Static/PANOPTIC_version_1.xlsx";
import tutorial_slides_prs from "../Static/PANOPTIC-tutorial-slides-prs.pptx";
import tutorial_pdf_prs from "../Static/PANOPTIC-tutorial-pdf-prs.pdf";
import panoptic_mtr from "../Static/PANOPTIC-MTR.pdf";
import panoptic_factsheet from "../Static/PANOPTIC-factsheet.pdf";

export default function PANOPTIC(props) {

    return (
        <div className='base-page'>
            <h4 className='heading new-section'>PANOPTIC™ Privacy Threat Model</h4>
            <hr style={{marginTop:'25px', marginBottom:'25px'}}></hr>
            <p>MITRE PANOPTIC™, the Pattern and Action Nomenclature Of Privacy Threats In Context, is a publicly-available privacy threat taxonomy for breaking down and describing privacy attacks against individuals and groups of individuals. PANOPTIC was generated from the bottom up  based on hundreds of real-world privacy attacks. It can be used for system and environmental privacy threat assessment, holistic privacy risk modeling, and privacy red teaming. As new and updated resources are developed and released, they will be posted here. </p>
            <p>Questions or feedback can be sent to <b>panoptic@mitre.org</b>.</p>
            <div class='paper'>
                <h5>
                <a href={panoptic_mtr} download="PANOPTIC-MTR.pdf">PANOPTIC™ MITRE Technical Report</a>
                </h5>
                <h6>Stuart Shapiro, Cara Bloom, Ben Ballard, Shelby Slotter, Mark Paes, Julie McEwen, Ryan Xu, and Samantha Katcher</h6>
            </div>
            <div class='paper'>
                <h5>
                <a href={panoptic_factsheet} download="PANOPTIC-factsheet.pdf">
                PANOPTIC™ Factsheet</a>
                </h5>
                <h6>Stuart Shapiro, Julie McEwen, Cara Bloom, Ben Ballard, Shelby Slotter, Samantha Katcher, Mark Paes, and Ryan Xu</h6>
            </div>
            <div class='paper'>
                <h5>
                <a href={panoptic_v1} download="PANOPTIC-version-1.xlsx">
                PANOPTIC™ Version 1</a>
                </h5>
                <h6>Stuart Shapiro, Julie McEwen, Cara Bloom, Ben Ballard, Shelby Slotter, Samantha Katcher, Mark Paes, and Ryan Xu</h6>
            </div>
            <div class='paper'>
                <h5>PANOPTIC™ Tutorial -
                <a href={tutorial_slides_prs} download="PANOPTIC-tutorial-slides-prs.pptx">
                &nbsp;Slides</a> |
                <a href={tutorial_pdf_prs} download="PANOPTIC-tutorial-pdf-prs.pdf">
                &nbsp;PDF</a>
                </h5>
                <h6>Stuart Shapiro, Julie McEwen, Ben Ballard, Shelby Slotter, and Samantha Katcher</h6>
            </div>
            <div class='paper'>
                <h5>
                PANOPTIC™ Version 2
                </h5>
                <h6>Coming Soon</h6>
            </div>
        </div>
    )
}
