import React from 'react';
import Agenda from './Agenda';

export default function Program(props) {

    let linddun = ' is a privacy threat modeling methodology that supports analysts in systematically eliciting and mitigating privacy threats in software architectures. LINDDUN provides support to guide you through the threat modeling process in a structured way. In addition, LINDDUN provides privacy knowledge support to enable also non-privacy experts to reason about privacy threats. LINDDUN is a mnemonic for the privacy threat categories it supports: linkability, identifiability, non-repudiation, detectability, disclosure of information, unawareness, and non-compliance.';
    let panoptic1 = 'MITRE is developing a taxonomy for describing and categorizing privacy threats, the Pattern and Action Nomenclature Of Privacy Threats In Context. The initial taxonomy was generated using a dataset of almost 150 privacy attacks identified from a set of US Federal Trade Commission (FTC) and Federal Communications Commission (FCC) privacy cases that did not involve data theft or breaches. The taxonomy includes a linear attack path component, similar to the ';
    let panoptic2 = ', and a contextual component that defines the socio-technical environment the attack took place in as well as the data types involved. PANOPTIC supports definition of Privacy Attack Patterns: generic instantiations of the types of attacks found in the FTC/FCC dataset. MITRE intends for PANOPTIC to be used to model privacy threats, integrate into and inform privacy risk management programs, and build a common knowledge base of privacy threats.';
    let breakout1 = 'How the community assesses the relative appropriateness of different degrees of privacy threat model generalization (specific to global)';
    let breakout2 = 'How to operationalize privacy threat models for threat assessment, risk modeling, and red teaming';

    return (
        <div className='base-page'>
            <h4 className='heading'>Program</h4>
            <div className='table'>
                <Agenda />
            </div>
            <div className='new-section'>
                <h4 className='heading'>Break-out Session Topics</h4>
                <ul>
                    <li>{breakout1}</li>
                    <li>{breakout2}</li>
                </ul>
            </div>
            <div className='new-section'>
                <h4 className='heading'>About the LINDDUN Privacy Threat Model</h4>
                <p>
                <a className='link' href='https://www.linddun.org' target='_blank' rel="noopener noreferrer">LINDDUN</a>
                {linddun}
                </p>
            </div>
            <div className='new-section'>
            <h4 className='heading'>About the MITRE PANOPTIC<sup>TM</sup> Privacy Threat Taxonomy</h4>
                <p> {panoptic1}
                <a className='link' href='https://attack.mitre.org' target='_blank' rel="noopener noreferrer">MITRE ATT&CK Framework</a>
                {panoptic2}
                </p>
            </div>
        </div>
    )
}